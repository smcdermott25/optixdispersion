/*
	Copyright (c) 2016, NVIDIA CORPORATION. All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions
	are met:
 *  * Redistributions of source code must retain the above copyright
	  notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
	  notice, this list of conditions and the following disclaimer in the
	  documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
	  contributors may be used to endorse or promote products derived
	  from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
	PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
	EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
	PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
	PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
	OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include "device_include/random.h"
#include "prd/prd.h"
#include "util/color/WavelengthToRGB.h"

using namespace optix;

rtBuffer<float2> lookup_table;
rtBuffer<float> light_color;

rtDeclareVariable(float3, shading_normal, attribute shading_normal,);
rtDeclareVariable(float3, front_hit_point, attribute front_hit_point,);
rtDeclareVariable(float3, back_hit_point, attribute back_hit_point,);


rtDeclareVariable(optix::Ray, ray, rtCurrentRay,);
rtDeclareVariable(float, t_hit, rtIntersectionDistance,);

rtDeclareVariable(float, scene_epsilon,,);

rtDeclareVariable(float3,       refraction_color,,);
rtDeclareVariable(float3,       reflection_color,,);

rtDeclareVariable(float3,       extinction,,);

rtDeclareVariable(PerRayData_radiance, prd_radiance, rtPayload,);
rtDeclareVariable(PerRayData_subray, prd_subray, rtPayload,);

rtDeclareVariable(uint, create_subrays,,);


// -----------------------------------------------------------------------------

static __device__ __inline__ float fresnel(float cos_theta_i, float cos_theta_t, float eta)
{
	const float rs = (cos_theta_i - cos_theta_t* eta) /
	                 (cos_theta_i + eta * cos_theta_t);
	const float rp = (cos_theta_i * eta - cos_theta_t) /
	                 (cos_theta_i * eta + cos_theta_t);

	return 0.5f * (rs * rs + rp * rp);
}

static __device__ float lookup(optix::buffer<float2>& table, float wavelength)
{
	int table_size = table.size();
	int max_index = table_size - 1;
	int min_index = 0;

	if (table_size < 1 || wavelength < table[0].x || wavelength > table[table_size - 1].x)
	{
		return 1.4f;
	}

	while (min_index <= max_index)
	{
		int current_index = (max_index + min_index) / 2;

		if (table[current_index].x == wavelength)
		{
			return table[current_index].y;
		}
		else if (table[current_index].x < wavelength)
		{
			min_index = current_index + 1;
		}
		else if (table[current_index].x > wavelength)
		{
			max_index = current_index - 1;
		}

	}

	//At this point wavelength is not in the table, if table[0]< wavelength < table[table_size-1] then we can interpolate
	//For this implementation, min_index ends up larger, so they're put in to new variables to avoid name confusion

	float2 larger = table[min_index];
	float2 smaller = table[max_index];


	float tableDifference = larger.x - smaller.x;
	float targetDifference = wavelength - smaller.x;
	float ratio = targetDifference / tableDifference;

	float iorDifference = larger.y - smaller.y;

	iorDifference *= ratio;

	return smaller.y + iorDifference;

}
// -----------------------------------------------------------------------------


static __device__ void disperse_ray(float3 w_out, PerRayData_subray& rayData, float3 normal)
{
	float cos_theta_i = optix::dot(w_out, normal);

	float eta;
	float3 transmittance = make_float3(1.0f);

	if (cos_theta_i > 0.0f)
	{
		// Ray is entering
		eta = lookup(lookup_table, rayData.wavelength);  // Note: does not handle nested dielectrics
	}
	else
	{
		// Ray is exiting; apply Beer's Law.
		// This is derived in Shirley's Fundamentals of Graphics book.
		transmittance = optix::expf(-extinction * t_hit);
		eta = 1.0f / lookup(lookup_table, rayData.wavelength);
		cos_theta_i = -cos_theta_i;
		normal = -normal;
	}

	float3 w_t;
	const bool tir = !optix::refract(w_t, -w_out, normal, eta);
	const float cos_theta_t = -optix::dot(normal, w_t);

	const float R = tir ? 1.0f : fresnel(cos_theta_i, cos_theta_t, eta);

	// Importance sample the Fresnel term
	const float z = rnd(rayData.seed);

	if (z <= R)
	{
		// Reflect
		const float3 fhp = rtTransformPoint(RT_OBJECT_TO_WORLD, front_hit_point);
		rayData.origin = fhp;
		rayData.direction = optix::reflect(-w_out, normal);
		rayData.reflectance *= reflection_color * transmittance;
	}
	else
	{
		// Refract
		const float3 bhp = rtTransformPoint(RT_OBJECT_TO_WORLD, back_hit_point);
		rayData.origin = bhp;
		rayData.direction = w_t;
		rayData.reflectance *= refraction_color * transmittance;
	}


	// Note: we do not trace the ray for the next bounce here, we just set it up for
	// the ray-gen program using per-ray data.

}

RT_PROGRAM void closest_hit_subray()
{
	float3 normal = normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, shading_normal));
	const float3 w_out = -ray.direction;
	disperse_ray(w_out, prd_subray, normal);
}

RT_PROGRAM void closest_hit_radiance()
{
	float3 normal = normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, shading_normal));
	const float3 w_out = -ray.direction;

	disperse_ray(w_out, prd_radiance, normal);

	if (create_subrays && prd_radiance.rayState != PerRayDataState::Dispersed)
	{
		for (int i = 0; i < MAXSUBRAYS; i++)
		{
			prd_radiance.subrayData[i] = prd_radiance;
			prd_radiance.subrayData[i].wavelength = light_color[i];
			// The colour is attenuated based on the number of subrays and a factor of 2.4 (to compensate for gamma correction)
			prd_radiance.subrayData[i].reflectance *= WavelengthToRGB(prd_radiance.subrayData[i].wavelength * 1000) * (2.4f / (MAXSUBRAYS - 1));
		}

		prd_radiance.rayState = PerRayDataState::Dispersed;
	}
}



