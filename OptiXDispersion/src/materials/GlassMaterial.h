#pragma once
#include <optix.h>
#include <optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>
#include "util/UtilFunctions.h"

const optix::float3 DEFAULT_TRANSMITTANCE = optix::make_float3(1.f, 1.f, 1.f);

optix::Material CreateGlassMaterial(optix::Context& context)
{
	optix::Program ch_program = context->createProgramFromPTXFile(PtxPath("glass.cu"), "closest_hit_radiance");
	optix::Program ch_program_subray = context->createProgramFromPTXFile(PtxPath("glass.cu"), "closest_hit_subray");

	optix::Material material = context->createMaterial();
	material->setClosestHitProgram(0, ch_program);
	material->setClosestHitProgram(1, ch_program_subray);

	material["fresnel_exponent"]->setFloat(4.0f);
	material["fresnel_minimum"]->setFloat(0.1f);
	material["fresnel_maximum"]->setFloat(1.0f);
	material["refraction_index"]->setFloat(1.4f);
	material["refraction_color"]->setFloat(0.99f, 0.99f, 0.99f);
	material["reflection_color"]->setFloat(0.99f, 0.99f, 0.99f);

	// Set this on the global context so it's easy to change in the gui
	const optix::float3 transmittance = DEFAULT_TRANSMITTANCE;
	context["unit_transmittance"]->setFloat(transmittance.x, transmittance.y, transmittance.z);
	const optix::float3 extinction = -logf(transmittance);
	context["extinction"]->setFloat(extinction);
	return material;
}