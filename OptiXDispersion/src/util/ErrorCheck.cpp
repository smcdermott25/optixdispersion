#include "ErrorCheck.h"
#include <stdio.h>
#include <iostream>

void reportErrorMessage(const char* message)
{
	std::cerr << "OptiX Error: '" << message << "'\n";
	OutputDebugString(message);
#if defined(_WIN32) && defined(RELEASE_PUBLIC)
	{
		char s[2048];
		sprintf(s, "OptiX Error: %s", message);
		MessageBox(0, s, "OptiX Error", MB_OK | MB_ICONWARNING | MB_SYSTEMMODAL);
	}
#endif
}

void handleError(optix::Context context, RTresult code, const char* file, int line)
{
	std::string message = context->getErrorString(code);
	message += "\n(" + std::string(file) + ":" + std::to_string(line) + ")";
	reportErrorMessage(message.c_str());
}


