#pragma once

#include <optix.h>
#include <optixu_math_namespace.h>
#include <optixu_matrix_namespace.h>

class Quaternion
{
public:

	Quaternion();

	Quaternion(float w, float x, float y, float z);

	Quaternion(optix::float4& quat);

	Quaternion(const optix::float3& from, const optix::float3& to);

	Quaternion(const Quaternion& a);

	Quaternion(float angle, const optix::float3& axis);

	// getters and setters
	void setW(float w);
	void setX(float x);
	void setY(float y);
	void setZ(float z);
	float w() const;
	float x() const;
	float y() const;
	float z() const;


	Quaternion& operator-=(const Quaternion& r);

	Quaternion& operator+=(const Quaternion& r);

	Quaternion& operator*=(const Quaternion& r);

	Quaternion& operator/=(const float a);

	Quaternion conjugate();

	void rotation(float& angle, optix::float3& axis) const;
	void rotation(float& angle, float& x, float& y, float& z) const;
	optix::Matrix4x4 rotationMatrix() const;
	optix::float4 ToVector4() const;
	optix::float3 ToVector3() const;

	// l2 norm
	float norm() const;

	float normalize();

private:
	optix::float4 q;
};

