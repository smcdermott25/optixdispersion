/*
    Copyright (c) 2016, NVIDIA CORPORATION. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
 *  * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
    OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Camera.h"
#include "Arcball.h"

//------------------------------------------------------------------------------
//
// Camera implementation
//
//------------------------------------------------------------------------------
//


Camera::Camera(unsigned int width, unsigned int height, const optix::float3& camera_eye, const optix::float3& camera_lookat,
               const optix::float3& camera_up, optix::Variable eye, optix::Variable u, optix::Variable v, optix::Variable w)
	:
	width(width),
	height(height),
	cameraEye(camera_eye),
	saveCameraLookat(camera_lookat),
	cameraLookat(camera_lookat),
	cameraUp(camera_up),
	cameraRotate(optix::Matrix4x4::identity()),
	variableEye(eye),
	variableU(u),
	variableV(v),
	variableW(w)
{
	Apply();
}

// Compute derived uvw frame and write to OptiX context
void Camera::Apply()
{
	const float vfov = 45.0f;
	const float aspectRatio = static_cast<float>(width) /
	                          static_cast<float>(height);

	optix::float3 cameraW;
	CalculateCameraVariables(cameraEye, cameraLookat, cameraUp, vfov, aspectRatio, cameraU, cameraV, cameraW, /*fov_is_vertical*/ true);

	const optix::Matrix4x4 frame = optix::Matrix4x4::fromBasis(normalize(cameraU), normalize(cameraV), normalize(-cameraW), cameraLookat);
	const optix::Matrix4x4 frame_inv = frame.inverse();
	// Apply camera rotation twice to match old SDK behavior
	const optix::Matrix4x4 trans = frame * cameraRotate * cameraRotate * frame_inv;

	cameraEye = optix::make_float3(trans * optix::make_float4(cameraEye, 1.0f));
	cameraLookat = optix::make_float3(trans * optix::make_float4(cameraLookat, 1.0f));
	cameraUp = optix::make_float3(trans * optix::make_float4(cameraUp, 0.0f));

	CalculateCameraVariables(cameraEye, cameraLookat, cameraUp, vfov, aspectRatio, cameraU, cameraV, cameraW, true);

	cameraRotate = optix::Matrix4x4::identity();

	// Write variables to OptiX context
	variableEye->setFloat(cameraEye);
	variableU->setFloat(cameraU);
	variableV->setFloat(cameraV);
	variableW->setFloat(cameraW);
}

void Camera::CalculateCameraVariables(optix::float3 eye, optix::float3 lookat, optix::float3 up, float fov, float aspectRatio,
                                      optix::float3& U, optix::float3& V, optix::float3& W, bool fovIsVertical)
{
	float ulen, vlen, wlen;
	W = lookat - eye; // Do not normalize W -- it implies focal length

	wlen = length(W);
	U = normalize(cross(W, up));
	V = normalize(cross(U, W));

	if (fovIsVertical)
	{
		vlen = wlen * tanf(0.5f * fov * M_PIf / 180.0f);
		V *= vlen;
		ulen = vlen * aspectRatio;
		U *= ulen;
	}
	else
	{
		ulen = wlen * tanf(0.5f * fov * M_PIf / 180.0f);
		U *= ulen;
		vlen = ulen / aspectRatio;
		V *= vlen;
	}
}

unsigned int Camera::Width() const
{
	return width;
}

unsigned int Camera::Height() const
{
	return height;
}

void Camera::ResetLookat()
{
	const optix::float3 translation = saveCameraLookat - cameraLookat;
	cameraEye += translation;
	cameraLookat = saveCameraLookat;
	Apply();
}

bool Camera::ProcessMouse(float x, float y, bool left_button_down, bool right_button_down, bool middle_button_down)
{
	static Arcball arcball;
	static optix::float2 mouse_prev_pos = optix::make_float2(0.0f, 0.0f);
	static bool   have_mouse_prev_pos = false;

	// No action if mouse did not move
	if ((mouse_prev_pos.x == x && mouse_prev_pos.y == y))
	{
		return false;
	}

	bool dirty = false;

	if (left_button_down || right_button_down || middle_button_down)
	{
		if (have_mouse_prev_pos)
		{
			if (left_button_down)
			{

				const optix::float2 from = { mouse_prev_pos.x, mouse_prev_pos.y };
				const optix::float2 to   = { x, y };

				const optix::float2 a = { from.x / width, from.y / height };
				const optix::float2 b = { to.x   / width, to.y   / height };

				cameraRotate = arcball.Rotate(b, a);

			}
			else if (right_button_down)
			{
				const float dx = (x - mouse_prev_pos.x) / width;
				const float dy = (y - mouse_prev_pos.y) / height;
				const float dmax = fabsf(dx) > fabs(dy) ? dx : dy;
				const float scale = fminf(dmax, 0.9f);

				cameraEye = cameraEye + (cameraLookat - cameraEye) * scale;

			}
			else if (middle_button_down)
			{
				const float dx = (x - mouse_prev_pos.x) / width;
				const float dy = (y - mouse_prev_pos.y) / height;

				optix::float3 translation = -dx * cameraU + dy * cameraV;
				cameraEye    = cameraEye + translation;
				cameraLookat = cameraLookat + translation;
			}

			Apply();
			dirty = true;

		}

		have_mouse_prev_pos = true;
		mouse_prev_pos.x = x;
		mouse_prev_pos.y = y;

	}
	else
	{
		have_mouse_prev_pos = false;
	}

	return dirty;
}

bool Camera::Rotate(float dx, float dy)
{
	static Arcball arcball;

	const optix::float2 from = optix::make_float2(width * .5f, height * .5f);
	const optix::float2 to   = optix::make_float2(from.x + dx, from.y + dy);

	const optix::float2 a = { from.x / width, from.y / height };
	const optix::float2 b = { to.x   / width, to.y   / height };

	cameraRotate = arcball.Rotate(b, a);
	Apply();

	return true;
}

bool Camera::Resize(unsigned int w, unsigned int h)
{
	if (w == width && h == height)
	{
		return false;
	}

	width = w;
	height = h;
	Apply();
	return true;
}


