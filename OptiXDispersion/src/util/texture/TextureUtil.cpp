#include "TextureUtil.h"



optix::TextureSampler LoadTexture(optix::Context context, const std::string& filename, optix::float3 default_color)
{
	return LoadPPMTexture(context, filename, default_color);
}
