#pragma once
#include <optix.h>
#include <optixpp_namespace.h>
#include "PPMLoader.h"

optix::TextureSampler LoadTexture(optix::Context context, const std::string& filename, optix::float3 default_color);