/*
    Copyright (c) 2016, NVIDIA CORPORATION. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
 *  * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
    OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Arcball.h"
#include "Quaternion.h"

Arcball::Arcball()
	:
	center(optix::make_float2(0.5f)),
	radius(0.45f)
{
}

Arcball::Arcball(const optix::float2& center, float radius)
	:
	center(center),
	radius(radius)
{
}

optix::float3 Arcball::ToSphere(const optix::float2& v) const
{
	float x = (v.x - center.x) / radius;
	float y = (1.0f - v.y - center.y) / radius;

	float z = 0.0f;
	float len2 = x * x + y * y;

	if (len2 > 1.0f)
	{
		// Project to closest point on edge of sphere.
		float len = sqrtf(len2);
		x /= len;
		y /= len;
	}
	else
	{
		z = sqrtf(1.0f - len2);
	}

	return optix::make_float3(x, y, z);
}


optix::Matrix4x4 Arcball::Rotate(const optix::float2& from, const optix::float2& to) const
{

	optix::float4 quaternionVector = optix::make_float4(cross(ToSphere(from), ToSphere(to)), dot(from, to));
	quaternionVector = normalize(quaternionVector);

	Quaternion q = Quaternion(quaternionVector);

	return q.rotationMatrix();
}

