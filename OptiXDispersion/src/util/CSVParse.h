#pragma once
#include <vector>
#include <string>
#include <filesystem>
#include <fstream>
#include <sstream>

typedef std::vector<optix::float2> IORTable;

IORTable ParseCSV(std::filesystem::path filePath)
{
	if (!std::filesystem::exists(filePath) || !std::filesystem::is_regular_file(filePath))
	{
		return IORTable();
	}

	std::ifstream file(filePath);
	std::stringstream buffer;
	buffer << file.rdbuf();
	std::string row;

	//skip the first row (contains headers)
	std::getline(buffer, row);
	IORTable table;

	while (std::getline(buffer, row))
	{
		size_t delimPos = row.find(',');
		float first = std::stof(row.substr(0, delimPos));
		float second = std::stof(row.substr(delimPos + 1, row.size()));

		optix::float2 entry = { first, second };

		table.push_back(entry);

	}

	return table;
}
