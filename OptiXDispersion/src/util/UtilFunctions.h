#pragma once
#include <string>

#include <optixu_math_namespace.h>

static std::string rootFolderPath;
static std::string PtxPath(const std::string& cudaFileName)
{
	if (rootFolderPath == "")
	{
		char resPath[512];
		GetModuleFileName(nullptr, resPath, 512);
		rootFolderPath = resPath;
		size_t pos = rootFolderPath.find_last_of('\\');
		rootFolderPath = rootFolderPath.substr(0, pos + 1);
	}

	return std::string(rootFolderPath) + "ptx/" + cudaFileName + ".ptx";
}


__inline__ optix::float3 logf(optix::float3 v)
{
	return optix::make_float3(logf(v.x), logf(v.y), logf(v.z));
}
