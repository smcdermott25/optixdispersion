/*
    Copyright (c) 2016, NVIDIA CORPORATION. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
 *  * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
    OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_matrix_namespace.h>

class Camera
{
public:

	Camera(unsigned int width, unsigned int height, const optix::float3& camera_eye, const optix::float3& camera_lookat,
	       const optix::float3& camera_up, optix::Variable eye, optix::Variable u, optix::Variable v, optix::Variable w);

	void ResetLookat();

	bool ProcessMouse(float x, float y, bool left_button_down, bool right_button_down, bool middle_button_down);

	bool Rotate(float dx, float dy);

	bool Resize(unsigned int w, unsigned int h);

	unsigned int Width() const;

	unsigned int Height() const;

private:

	// Compute derived uvw frame and write to OptiX context
	void Apply();
	void Camera::CalculateCameraVariables(optix::float3 eye, optix::float3 lookat, optix::float3 up, float fov, float aspectRatio,
	                                      optix::float3& U, optix::float3& V, optix::float3& W, bool fovIsVertical);

	unsigned int width;
	unsigned int height;

	optix::float3    cameraEye;
	optix::float3    cameraLookat;
	const optix::float3 saveCameraLookat;
	optix::float3    cameraUp;
	optix::float3    cameraU; // derived
	optix::float3    cameraV; // derived
	optix::Matrix4x4 cameraRotate;

	// Handles for setting derived values for OptiX context
	optix::Variable  variableEye;
	optix::Variable  variableU;
	optix::Variable  variableV;
	optix::Variable  variableW;

};


