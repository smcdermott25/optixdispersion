#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <stdlib.h>
#include "ErrorCheck.h"

optix::Buffer CreateOutputBuffer(optix::Context& context, RTformat format, unsigned width, unsigned height, bool use_pbo)
{

	optix::Buffer buffer;

	if (use_pbo)
	{
		// First allocate the memory for the GL buffer, then attach it to OptiX.

		// Assume ubyte4 or float4 for now
		unsigned int elmt_size = format == RT_FORMAT_UNSIGNED_BYTE4 ? 4 : 16;
		int size = elmt_size * width * height;
		GLuint vbo = 0;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, size, 0, GL_STREAM_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		buffer = context->createBufferFromGLBO(RT_BUFFER_OUTPUT, vbo);
		buffer->setFormat(format);
		buffer->setSize(width, height);
	}
	else
	{
		buffer = context->createBuffer(RT_BUFFER_OUTPUT, format, width, height);
	}

	return buffer;
}