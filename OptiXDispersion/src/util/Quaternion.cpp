#include "Quaternion.h"
#include <optixu_quaternion.h>
Quaternion::Quaternion()
{
	q = optix::make_float4(0);
}

Quaternion::Quaternion(float x, float y, float z, float w)
{
	q.x = x;
	q.y = y;
	q.z = z;
	q.w = w;
}

Quaternion::Quaternion(optix::float4& quat)
{
	q = quat;
}

Quaternion::Quaternion(const Quaternion& a)
{
	q = a.ToVector4();
}
Quaternion::Quaternion(const optix::float3& from, const optix::float3& to)
{
	const optix::float3 c = cross(from, to);

	q.x = c.x;
	q.y = c.y;
	q.z = c.z;
	q.w = dot(from, to);
}


Quaternion::Quaternion(float angle, const optix::float3&  axis)
{
	const float  n = length(axis);
	const float  inverse = 1.0f / n;
	const optix::float3 naxis = axis * inverse;
	const float  s = sinf(angle / 2.0f);

	q.w = naxis.x * s * inverse;
	q.x = naxis.y * s * inverse;
	q.y = naxis.z * s * inverse;
	q.z = cosf(angle / 2.0f);
}

void Quaternion::setW(float w)
{
	q.w = w;
}

void Quaternion::setX(float x)
{
	q.x = x;
}

void Quaternion::setY(float y)
{
	q.y = y;
}

void Quaternion::setZ(float z)
{
	q.z = z;
}

float Quaternion::w() const
{
	return q.w;
}

float Quaternion::x() const
{
	return q.x;
}

float Quaternion::y() const
{
	return q.y;
}

float Quaternion::z() const
{
	return q.z;
}


Quaternion Quaternion::conjugate()
{
	optix::float4 vec = q;
	vec.x = -vec.x;
	vec.y = -vec.y;
	vec.z = -vec.z;
	return Quaternion(vec);
}

void Quaternion::rotation(float& angle, optix::float3& axis) const
{
	Quaternion n = *this;
	n.normalize();
	optix::float4 vec = n.ToVector4();
	axis.x = vec.x;
	axis.y = vec.y;
	axis.z = vec.z;
	angle = 2.0f * acosf(vec.w);
}

void Quaternion::rotation(float& angle, float& x, float& y, float& z) const
{
	Quaternion n = *this;
	n.normalize();
	optix::float4 vec = n.ToVector4();
	x = vec.x;
	y = vec.y;
	z = vec.z;
	angle = 2.0f * acosf(vec.w);
}

float Quaternion::norm() const
{
	return sqrtf(q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w);
}

float Quaternion::normalize()
{
	float n = norm();
	float inverse = 1.0f / n;
	q.x *= inverse;
	q.y *= inverse;
	q.z *= inverse;
	q.w *= inverse;
	return n;
}


float dot(const Quaternion& l, const Quaternion& r)
{
	return l.w() * r.w() + l.x() * r.x() + l.y() * r.y() + l.z() * r.z();
}


optix::Matrix4x4 Quaternion::rotationMatrix() const
{
	optix::Matrix4x4 m;

	const float qw = q.w;
	const float qx = q.x;
	const float qy = q.y;
	const float qz = q.z;

	m[0 * 4 + 0] = 1.0f - 2.0f * qy * qy - 2.0f * qz * qz;
	m[0 * 4 + 1] = 2.0f * qx * qy - 2.0f * qz * qw;
	m[0 * 4 + 2] = 2.0f * qx * qz + 2.0f * qy * qw;
	m[0 * 4 + 3] = 0.0f;

	m[1 * 4 + 0] = 2.0f * qx * qy + 2.0f * qz * qw;
	m[1 * 4 + 1] = 1.0f - 2.0f * qx * qx - 2.0f * qz * qz;
	m[1 * 4 + 2] = 2.0f * qy * qz - 2.0f * qx * qw;
	m[1 * 4 + 3] = 0.0f;

	m[2 * 4 + 0] = 2.0f * qx * qz - 2.0f * qy * qw;
	m[2 * 4 + 1] = 2.0f * qy * qz + 2.0f * qx * qw;
	m[2 * 4 + 2] = 1.0f - 2.0f * qx * qx - 2.0f * qy * qy;
	m[2 * 4 + 3] = 0.0f;

	m[3 * 4 + 0] = 0.0f;
	m[3 * 4 + 1] = 0.0f;
	m[3 * 4 + 2] = 0.0f;
	m[3 * 4 + 3] = 1.0f;

	return m;
}

optix::float4 Quaternion::ToVector4() const
{
	return q;
}

optix::float3 Quaternion::ToVector3() const
{
	return optix::make_float3(q.x, q.y, q.z);
}


Quaternion& Quaternion::operator*=(const Quaternion& r)
{

	float w = q.w * r.w() - q.x * r.x() - q.y * r.y() - q.z * r.z();
	float x = q.w * r.x() + q.x * r.w() + q.y * r.z() - q.z * r.y();
	float y = q.w * r.y() + q.y * r.w() + q.z * r.x() - q.x * r.z();
	float z = q.w * r.z() + q.z * r.w() + q.x * r.y() - q.y * r.x();

	q.w = w;
	q.x = x;
	q.y = y;
	q.z = z;
	return *this;
}


Quaternion& Quaternion::operator/=(const float a)
{
	float inverse = 1.0f / a;
	q.x *= inverse;
	q.y *= inverse;
	q.z *= inverse;
	q.w *= inverse;
	return *this;
}

Quaternion& Quaternion::operator-=(const Quaternion& r)
{
	q.x -= r.x();
	q.y -= r.y();
	q.z -= r.z();
	q.w -= r.w();
	return *this;
}

Quaternion& Quaternion::operator+=(const Quaternion& r)
{
	q.x += r.x();
	q.y += r.y();
	q.z += r.z();
	q.w += r.w();
	return *this;
}

Quaternion operator*(const float a, const Quaternion& r)
{
	return Quaternion(a * r.x(), a * r.y(), a * r.z(), a * r.w());
}


Quaternion operator*(const Quaternion& r, const float a)
{
	return a * r;
}


Quaternion operator/(const Quaternion& r, const float a)
{
	return (1.0f / a) * r;
}


Quaternion operator/(const float a, const Quaternion& r)
{
	return r / a;
}


Quaternion operator-(const Quaternion& l, const Quaternion& r)
{
	return Quaternion(l.x() - r.x(), l.y() - r.y(), l.z() - r.z(), l.w() - r.w());
}


bool operator==(const Quaternion& l, const Quaternion& r)
{
	return (l.x() == r.x() && l.y() == r.y() && l.z() == r.z() && l.w() == r.w());
}


bool operator!=(const Quaternion& l, const Quaternion& r)
{
	return !(l == r);
}


Quaternion operator+(const Quaternion& l, const Quaternion& r)
{
	return Quaternion(l.x() + r.x(), l.y() + r.y(), l.z() + r.z(), l.w() + r.w());
}


Quaternion operator*(const Quaternion& l, const Quaternion& r)
{
	float w = l.w() * r.w() - l.x() * r.x() - l.y() * r.y() - l.z() * r.z();
	float x = l.w() * r.x() + l.x() * r.w() + l.y() * r.z() - l.z() * r.y();
	float y = l.w() * r.y() + l.y() * r.w() + l.z() * r.x() - l.x() * r.z();
	float z = l.w() * r.z() + l.z() * r.w() + l.x() * r.y() - l.y() * r.x();
	return Quaternion(x, y, z, w);
}

