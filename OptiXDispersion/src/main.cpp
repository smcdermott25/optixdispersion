//openGL headers
#  include <GL/glew.h>
#if defined(_WIN32)
	#include <GL/wglew.h>
#endif
#include <GLFW/glfw3.h>

//optix headers
#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_aabb_namespace.h>
#include <optixu/optixu_matrix_namespace.h>

//optix C++ wrapper header
#include <optixu/optixpp_namespace.h>

//imgui headers
#include <imgui.h>
#include <imgui_impl_glfw_gl3.h>

//standard library headers
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <filesystem>
#include <map>
#include <algorithm>
#include <time.h>

#include "util/OutputBuffer.h"
#include "util/texture/TextureUtil.h"
#include "util/mesh/OptiXMesh.h"
#include "util/Camera.h"
#include "util/CSVParse.h"
#include "util/UtilFunctions.h"
#include "materials/GlassMaterial.h"
#include "prd/prd.h"

GLFWwindow* window;
bool glfwInitialized = false;

const unsigned int MAXIMUM_DEPTH = 100u;

optix::Material glassMaterial;
const unsigned int WIDTH = 1920;
const unsigned int HEIGHT = 1080u;

std::map<std::string, IORTable> IORTables;
std::vector<optix::GeometryGroup> geometryGroups;
optix::Group topGroup;

#if MAXSUBRAYS == 4
float light_color[] = {0.463f, 0.505f, 0.644f, 0.680f };
#elif MAXSUBRAYS == 6
float light_color[] = { 0.419f, 0.449f, 0.507f, 0.519f, 0.607f, 0.672f };
#elif MAXSUBRAYS == 9
float light_color[] = { 0.411f, 0.417f, 0.453f, 0.484f, 0.514f,  0.544f, 0.599f, 0.606f,  0.668f};
#endif

struct CallbackData
{
	Camera& camera;
	unsigned int& accumulation_frame;
};

void ErrorCallback(int error, const char* description)
{
	std::cerr << "GLFW Error " << error << ": " << description << std::endl;
}

void KeyCallback(GLFWwindow* /*window*/, int key, int /*scancode*/, int action, int /*mods*/)
{
	if (action == GLFW_PRESS)
	{
		switch (key)
		{
		case GLFW_KEY_Q:
		case GLFW_KEY_ESCAPE:
			ImGui_ImplGlfwGL3_Shutdown();
			ImGui::DestroyContext();

			if (window)
			{
				glfwDestroyWindow(window);
			}

			glfwTerminate();

			exit(EXIT_SUCCESS);
		}
	}
}

GLFWwindow* InitGLFW()
{
	// Initialize GLFW
	if (!glfwInit())
	{
		throw optix::Exception("glfwInit failed.");
	}

	glfwSetErrorCallback(ErrorCallback);

	window = glfwCreateWindow(600, 600, "", NULL, NULL);

	if (!window)
	{
		throw optix::Exception("GLFW window or GL context creation failed.");
	}

	glfwMakeContextCurrent(window);
	glfwSetWindowPos(window, 0, 30);

	glfwSetKeyCallback(window, KeyCallback);

	glfwInitialized = true;

	ImGui::CreateContext();

	ImGui_ImplGlfwGL3_Init(window, true);
	int w, h;

	glfwGetWindowSize(window, &w, &h);
	return window;
}

void InitGLEW()
{
	GLenum err = glewInit();

	if (err != GLEW_OK)
	{
		std::cerr << "GLEW init failed: " << glewGetErrorString(err) << std::endl;
		exit(EXIT_FAILURE);
	}
}

void CheckBuffer(const optix::Buffer& buffer)
{
	// Check to see if the buffer is two dimensional
	unsigned int dimensionality = buffer->getDimensionality();

	if (dimensionality != 2)
	{
		throw optix::Exception("Attempting to display non-2D buffer");
	}

	// Check to see if the buffer is of type float{1,3,4} or uchar4
	RTformat format = buffer->getFormat();

	if (RT_FORMAT_FLOAT != format &&
	    RT_FORMAT_FLOAT4 != format &&
	    RT_FORMAT_FLOAT3 != format &&
	    RT_FORMAT_UNSIGNED_BYTE4 != format)
	{
		throw optix::Exception("Attempting to display buffer with format not float, float3, float4, or uchar4");
	}
}

void Display(optix::Buffer& imageBuffer)
{
	RTsize rtWidth, rtHeight;
	imageBuffer->getSize(rtWidth, rtHeight);
	const GLsizei width = static_cast<GLsizei>(rtWidth);
	const GLsizei height = static_cast<GLsizei>(rtHeight);

	GLenum glDataType;
	GLenum glFormat;

	switch (imageBuffer->getFormat())
	{
	case RT_FORMAT_UNSIGNED_BYTE4:
		glDataType = GL_UNSIGNED_BYTE;
		glFormat = GL_BGRA;
		break;

	case RT_FORMAT_FLOAT:
		glDataType = GL_FLOAT;
		glFormat = GL_LUMINANCE;
		break;

	case RT_FORMAT_FLOAT3:
		glDataType = GL_FLOAT;
		glFormat = GL_RGB;
		break;

	case RT_FORMAT_FLOAT4:
		glDataType = GL_FLOAT;
		glFormat = GL_RGBA;
		break;

	default:
		fprintf(stderr, "Unrecognized buffer data type or format.\n");
		exit(2);
		break;
	}

	GLvoid* imageData = imageBuffer->map();
	glDrawPixels(width, height, glFormat, glDataType, imageData);  // Using default glPixelStore unpack alignment of 4.

	// Now unmap the buffer
	imageBuffer->unmap(0);

	ImGui::Render();
	ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());

	glfwSwapBuffers(window);
}

void SetGlassMaterialLookup(const std::string& tableName, optix::Context& context)
{
	IORTable newTable = IORTables[tableName];
	optix::Buffer tableBuffer = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT2, newTable.size());
	optix::float2* tableData = static_cast<optix::float2*>(tableBuffer->map());
	std::copy(newTable.begin(), newTable.end(), tableData);

	glassMaterial["lookup_table"]->setBuffer(tableBuffer);

	tableBuffer->unmap();
}

void SetGlassMaterialLight(float newColors[], optix::Context& context)
{
	optix::Buffer lightBuffer = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT, MAXSUBRAYS);
	float* data = static_cast<float*>(lightBuffer->map());
	std::copy(newColors, newColors + MAXSUBRAYS, data);
	glassMaterial["light_color"]->setBuffer(lightBuffer);

	lightBuffer->unmap();
}


void DisplayBufferGLFW(const char* windowTitle, Camera& camera, optix::Context& context)
{
	if (!glfwInitialized)
	{
		throw optix::Exception("displayGLFWWindow called before initGLFW.");
	}

	glfwSetWindowTitle(window, windowTitle);
	glfwSetWindowSize(window, WIDTH, HEIGHT);

	// Init state
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 1, 0, 1, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glViewport(0, 0, WIDTH, HEIGHT);

	unsigned int frame_count = 0;
	unsigned int output_frames = 0;
	time_t lastTime = time(nullptr);
	double dt = 0.0;
	unsigned int accumulation_frame = 0;
	optix::float3 glass_transmittance = DEFAULT_TRANSMITTANCE;
	float log_transmittance_depth = 0.0f;
	int currentModel = 0;
	int max_depth = 10;
	bool createSubrays = true;
	CallbackData cb = { camera, accumulation_frame };
	glfwSetWindowUserPointer(window, &cb);
	optix::Group topGroup = context["top_object"]->getGroup();
	optix::Buffer outputBuffer = context["output_buffer"]->getBuffer();
	int currentLookupTable = 0;
	int radioSelection = 0;

	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
		ImGui_ImplGlfwGL3_NewFrame();
		time_t now = time(nullptr);
		dt += difftime(now, lastTime);
		lastTime = now;
		frame_count++;
		ImGuiIO& io = ImGui::GetIO();

		// Let imgui process the mouse first
		if (!io.WantCaptureMouse)
		{
			double x, y;
			glfwGetCursorPos(window, &x, &y);

			if (camera.ProcessMouse((float)x, (float)y, ImGui::IsMouseDown(0), ImGui::IsMouseDown(1), ImGui::IsMouseDown(2)))
			{
				accumulation_frame = 0;
			}
		}


		// imgui pushes
		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0.5f, 0.5f));
		ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 0.6f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 2.0f);

		static const ImGuiWindowFlags windowFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoScrollbar;

		ImGui::SetNextWindowPos(ImVec2(2.0f, 40.0f));
		ImGui::Begin("controls", 0, windowFlags);

		if (ImGui::CollapsingHeader("Controls", ImGuiTreeNodeFlags_DefaultOpen))
		{
			bool transmittance_changed = false;

			if (ImGui::ColorEdit3("transmittance color", (float*)(&glass_transmittance.x)))
			{
				transmittance_changed = true;
			}

			if (ImGui::SliderFloat("log transmittance depth", (float*)(&log_transmittance_depth), -8, 8))
			{
				transmittance_changed = true;
			}

			if (transmittance_changed)
			{
				const float t0 = expf(log_transmittance_depth);
				const optix::float3 extinction = -logf(glass_transmittance) / t0;
				context["extinction"]->setFloat(extinction);
				accumulation_frame = 0;
			}

			if (ImGui::SliderInt("max depth", &max_depth, 0, MAXIMUM_DEPTH))
			{
				context["max_depth"]->setInt(max_depth);
				accumulation_frame = 0;
			}

			if (ImGui::Button("cycle model"))
			{
				currentModel++;

				if (currentModel >= geometryGroups.size())
				{
					currentModel = 0;
				}

				context["top_object"]->set(geometryGroups[currentModel]);


				accumulation_frame = 0;
			}

			if (currentModel <= 1)
			{
				ImGui::Text("RTX Enabled");
			}
			else
			{
				ImGui::Text("RTX Disabled");
			}
		}

		if (ImGui::CollapsingHeader("Light Options", ImGuiTreeNodeFlags_DefaultOpen))
		{
			bool light_color_changed = false;

			if (ImGui::Checkbox("Create Sub-rays", &createSubrays))
			{
				context["create_subrays"]->setUint(createSubrays);
				accumulation_frame = 0;
			}

			for (int i = 0; i < MAXSUBRAYS; i++)
			{
				std::string label = "Ray Wavelength " + std::to_string(i + 1);

				if (ImGui::SliderFloat(label.c_str(), (float*)&light_color[i], 0.4f, 0.7f))
				{
					light_color_changed = true;
					accumulation_frame = 0;

				}

			}

			if (light_color_changed)
			{
				SetGlassMaterialLight(light_color, context);
			}

			ImGui::RadioButton("Fused Quartz", &radioSelection, 0);
			ImGui::SameLine();
			ImGui::RadioButton("Diamond", &radioSelection, 1);
			ImGui::SameLine();
			ImGui::RadioButton("Water", &radioSelection, 2);
			ImGui::SameLine();
			ImGui::RadioButton("None", &radioSelection, 3);

			if (currentLookupTable != radioSelection)
			{
				switch (radioSelection)
				{
				case 0:
					SetGlassMaterialLookup("fused_quartz", context);
					break;

				case 1:
					SetGlassMaterialLookup("diamond", context);
					break;

				case 2:
					SetGlassMaterialLookup("water", context);
					break;


				case 3:
					SetGlassMaterialLookup("None", context);
					break;
				}

				accumulation_frame = 0;
				currentLookupTable = radioSelection;
			}
		}

		ImGui::Value("Accumulation Frames", accumulation_frame);
		ImGui::Value("FPS", output_frames);
		ImGui::End();

		ImGui::PopStyleVar(3);

		context["frame"]->setUint(accumulation_frame < MAXSAMPLES ? accumulation_frame++ : accumulation_frame);
		context->launch(0, camera.Width(), camera.Height());

		Display(outputBuffer);

		if (dt >= 1)
		{
			dt = 0;
			output_frames = frame_count;
			frame_count = 0;
		}


	}

	ImGui_ImplGlfwGL3_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();
}

optix::GeometryGroup LoadModel(const std::string& sourceFolder, optix::Context context, optix::Aabb& aabb, optix::Material material, bool useTriangles = true)
{

	const std::string ptx_path = PtxPath("triangle_mesh.cu");
	std::filesystem::path folderPath = sourceFolder;

	int num_triangles = 0;
	const optix::Matrix4x4 xform = optix::Matrix4x4::rotate(-M_PIf / 2.0f, optix::make_float3(0.0f, 1.0f, 0.0f));
	std::vector<std::string>files;
	std::vector<optix::Matrix4x4> meshTransforms;

	if (std::filesystem::exists(folderPath) && std::filesystem::is_directory(folderPath))
	{
		for (auto& entry : std::filesystem::directory_iterator(folderPath))
		{
			if (entry.is_regular_file())
			{
				files.push_back(entry.path().string());
				meshTransforms.push_back(xform);
			}
		}
	}

	optix::Group topGroup = context["top_object"]->getGroup();

	optix::GeometryGroup geometry_group = context->createGeometryGroup();
	geometry_group->setAcceleration(context->createAcceleration("Trbvh"));

	topGroup->addChild(geometry_group);

	for (size_t i = 0; i < files.size(); ++i)
	{

		OptiXMesh mesh;
		mesh.context = context;

		// override defaults
		mesh.intersection = context->createProgramFromPTXFile(ptx_path, "mesh_intersect_refine");
		mesh.bounds = context->createProgramFromPTXFile(ptx_path, "mesh_bounds");
		mesh.material = material;

		if (useTriangles)
		{
			loadMeshTriangles(files[i], mesh, meshTransforms[i]);
		}
		else
		{
			loadMeshGeometry(files[i], mesh, meshTransforms[i]);
		}

		geometry_group->addChild(mesh.geom_instance);

		aabb.include(mesh.bbox_min, mesh.bbox_max);

		std::cerr << files[i] << ": " << mesh.num_triangles << std::endl;
		num_triangles += mesh.num_triangles;
	}

	std::cerr << "Total triangle count: " << num_triangles << std::endl;
	return geometry_group;
}

optix::Aabb LoadScene(optix::Context& context)
{

	topGroup = context->createGroup();
	topGroup->setAcceleration(context->createAcceleration("Trbvh"));
	context["top_object"]->set(topGroup);

	glassMaterial = CreateGlassMaterial(context);

	optix::Aabb aabb;

	geometryGroups.push_back(LoadModel(rootFolderPath + "res/models/dragon", context, aabb, glassMaterial));
	geometryGroups.push_back(LoadModel(rootFolderPath + "res/models/teapot", context, aabb, glassMaterial));
	geometryGroups.push_back(LoadModel(rootFolderPath + "res/models/dragon", context, aabb, glassMaterial, false));
	geometryGroups.push_back(LoadModel(rootFolderPath + "res/models/teapot", context, aabb, glassMaterial, false));

	context["top_object"]->set(geometryGroups[0]);

	IORTable fusedQuartzTable = ParseCSV(std::filesystem::path(rootFolderPath + "/res/refraction_tables/fused_quartz.csv"));
	IORTables["fused_quartz"] = fusedQuartzTable;
	IORTable diamondTable = ParseCSV(std::filesystem::path(rootFolderPath + "/res/refraction_tables/diamond.csv"));
	IORTables["diamond"] = diamondTable;
	IORTable waterTable = ParseCSV(std::filesystem::path(rootFolderPath + "/res/refraction_tables/water.csv"));
	IORTables["water"] = waterTable;

	optix::Buffer tableBuffer = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT2, fusedQuartzTable.size());

	optix::float2* tableData = static_cast<optix::float2*>(tableBuffer->map());

	std::copy(fusedQuartzTable.begin(), fusedQuartzTable.end(), tableData);
	glassMaterial["lookup_table"]->setBuffer(tableBuffer);

	SetGlassMaterialLight(light_color, context);

	tableBuffer->unmap();

	return aabb;
}


optix::Context CreateContext(bool use_pbo)
{
	// Set up context
	optix::Context context = optix::Context::create();
	context->setRayTypeCount(2);
	context->setEntryPointCount(1);
	context->setPrintEnabled(true);
	context->setPrintBufferSize(1024);
	// Note: this sample does not need a big stack size even with high ray depths,
	// because rays are not shot recursively.
	context->setStackSize(1000);

	// Note: high max depth for reflection and refraction through glass
	context["max_depth"]->setInt(10);
	context["cutoff_color"]->setFloat(0.2f, 0.2f, 0.2f);
	context["frame"]->setUint(0u);
	context["scene_epsilon"]->setFloat(1.e-3f);
	context["create_subrays"]->setUint(true);
	optix::Buffer buffer = CreateOutputBuffer(context, RT_FORMAT_UNSIGNED_BYTE4, WIDTH, HEIGHT, use_pbo);
	context["output_buffer"]->set(buffer);

	// Accumulation buffer
	optix::Buffer accum_buffer = context->createBuffer(RT_BUFFER_INPUT_OUTPUT | RT_BUFFER_GPU_LOCAL,
	                             RT_FORMAT_FLOAT4, WIDTH, HEIGHT);
	context["accum_buffer"]->set(accum_buffer);

	// Ray generation program
	optix::Program ray_gen_program = context->createProgramFromPTXFile(PtxPath("path_trace_camera.cu"), "pinhole_camera");
	context->setRayGenerationProgram(0, ray_gen_program);

	// Exception program
	optix::Program exception_program = context->createProgramFromPTXFile(PtxPath("path_trace_camera.cu"), "exception");
	context->setExceptionProgram(0, exception_program);

	context["bad_color"]->setFloat(1.0f, 0.0f, 1.0f);

	// Miss program
	context->setMissProgram(0, context->createProgramFromPTXFile(PtxPath("gradientbg.cu"), "miss"));

	context->setMissProgram(1, context->createProgramFromPTXFile(PtxPath("gradientbg.cu"), "miss_subray"));

	context["background_light"]->setFloat(0.7f, 0.7f, 0.7f);
	context["background_dark"]->setFloat(0.3f, 0.3f, 0.3f);

	// align background's up direction with camera's look direction
	optix::float3 bg_up = normalize(optix::make_float3(0.0f, -1.0f, -1.0f));

	// tilt the background's up direction in the direction of the camera's up direction
	bg_up.y += 1.0f;
	bg_up = normalize(bg_up);
	context["up"]->setFloat(bg_up.x, bg_up.y, bg_up.z);

	return context;
}

void Run(char* windowName)
{
	char resPath[512];
	GetModuleFileName(nullptr, resPath, 512);
	rootFolderPath = resPath;
	size_t pos = rootFolderPath.find_last_of('\\');
	rootFolderPath = rootFolderPath.substr(0, pos + 1);
	optix::Context context;


	try
	{
		InitGLFW();
		InitGLEW();

		context = CreateContext(true);
		optix::Aabb aabb = LoadScene(context);

		context->validate();

		const optix::float3 cameraEye(optix::make_float3(0.0f, 1.5f * aabb.extent(1), 1.5f * aabb.extent(2)));
		const optix::float3 cameraLookat(aabb.center());
		const optix::float3 cameraUp(optix::make_float3(0.0f, 1.0f, 0.0f));

		Camera camera(WIDTH, HEIGHT, cameraEye, cameraLookat, cameraUp, context["eye"], context["U"], context["V"], context["W"]);


		DisplayBufferGLFW(windowName, camera, context);

	}

	CATCH(context)
}

int main(int argc, char* argv[])
{
	Run(argv[0]);
	return 0;
}
